#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main()
{
    /* Ci dessous on d�clares les variables et constantes.
    MIN repr�sente le nombre minimum que l'ordinateur peut choisir*/

    const MIN = 1;
    int nombreMystere = 0, nombreEntre = 0, nombreMax = 0, difficulte = 0;
    int compteur = 0, continuerPartie = 1;


    printf("Bienvenue dans le jeu du Plus ou Moins !\n\n");

    do // on entre dans la boucle d'une partie
    {
        printf("=== MENU ===\n");
        printf("Choisir le mode de difficulte :\n\n");
        printf("1. Facile - Nombre mystere entre 1 et 100\n");
        printf("2. Moyen  - Nombre mystere entre 1 et 1000\n");
        printf("3. Difficile - Nombre mystere entre 1 et 10000\n\n");


        /*Dessous : Boucle pour la selection du mode de difficult�, et �vitez le choix
        d'un mode qui n'existe pas.*/
        do
        {
            printf("Entre le mode choisie : ");
            scanf("%d", &difficulte);

            switch (difficulte)
            {
            case 1 :
                nombreMax = 100;
                break;
            case 2 :
                nombreMax = 1000;
                break;
            case 3:
                nombreMax = 10000;
                break;
            default:
                printf("\nMerci de selectionner le niveau de difficulte 1,2 ou 3\n");
                break;
            }
        }
        while (difficulte > 3 || difficulte == 0);


        /* Ci dessous la fonction fournie qui permet
        de generer un nombre al�atoire entre 1 et 10000 selon le choix
        de difficult� */

        srand(time(NULL));
        nombreMystere = (rand()% (nombreMax - MIN +1)) + MIN;


        /* Dessous : Boucle qui permet de v�rifier si le nombre saisie par l'utilisateur
        est �gal au nombre mystere, s'il est plus bas, ou plus haut. Partie gagn�
        quand les nombres sont �gaux*/
        do
        {
            printf("\nQuel est le nombre mystere ?");
            scanf("%d", &nombreEntre);
            compteur++; // Pour donner au joueur le nombre de coup qu'il lui a fallu pour gagner

            if(nombreEntre < nombreMystere)
            {
                printf("C'est plus !");
            }
            else if (nombreEntre > nombreMystere)
            {
                printf("C'est moins !");
            }

        }
        while(nombreEntre != nombreMystere);

        printf("Bravo, vous avez trouve le nombre mystere en %d coups !!!\n\n", compteur);
        printf("Voulez vous faire une autre partie ?\n");
        printf("1.Oui\n");
        printf("2.Non\n");
        scanf("%d", &continuerPartie); // on demande au joueur s'il veut rejouer et relancer une boucle de partie.

        if(continuerPartie==1)
        {
            continuerPartie=1;
        }
        else
        {
            continuerPartie=0;
        }

        compteur=0; // remise du compteur a z�ro pour la nouvelle partie s'il y a.

    }
    while (continuerPartie == 1);


    return 0;
}
